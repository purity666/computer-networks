from multiprocessing import Process, Manager
import os
import sys
import logging
import argparse

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
import src
from src.paxos_node import Node

logger = logging.getLogger('paxos.controller')


def run_paxos(num_paxos_nodes):

    with Manager() as manager:

        node_map = {}

        # create the required number of nodes
        for i in range(num_paxos_nodes):
            node_map[i] = Node(_id=i, manager=manager)

        # pass all the nodes to every-other node for their
        # communication via 'manager' proxy objects
        for i in range(num_paxos_nodes):
            node_map[i].set_majority({j: node for j, node in node_map.items()})

        master = Process(target=node_map[0].save_value, args=("START",))
        master.start()
        master.join()

        while True:
            telling = input("Tell something: ")
            proposer = Process(target=node_map[0].save_value, args=(telling,))
            proposer.start()
            proposer.join()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-npn',
        '--num_of_paxos_nodes',
        type=int,
        default=3,
        help='number of paxos-nodes to create, DEFAULT[3]'
    )

    args = parser.parse_args()

    run_paxos(args.num_of_paxos_nodes)
