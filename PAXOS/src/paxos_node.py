import logging
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
import src

from random import random
import time

from ctypes import c_char_p

logger = logging.getLogger('paxos.node_id_{}'.format(os.getpid()))

class Value:        
    def __init__(self, value=None):
        if value:
            self.value = value
        else:
            self.value = None

def p():
    return random()

class Node():
    def __init__(self, _id, manager, prob=0.5):
        self.id = manager.Value('i', _id)
        self.nodes = manager.dict()
        self.data_store = manager.dict()

        self.prob = prob #Lost probability
        self.num_nodes = manager.Value('i', None)
        self.majority = manager.Value('i', None)
        self.paxos_proposed_id = manager.Value('i', None)
        self.paxos_promised_id = manager.Value('i', -1)
        self.paxos_proposed_value = manager.Value('i', None)
        self.paxos_accepted_id = manager.Value('i', -1)
        self.paxos_accepted_value = manager.Value(c_char_p, None)

    def set_majority(self, nodes):
        for k, v in nodes.items():
            self.nodes[k] = v
        self.majority.value = int((len(self.nodes)) / 2) + 1
        self.num_nodes.value = len(self.nodes)

    def generate_next_paxos_id(self):
        if self.paxos_proposed_id.value is None:
            self.paxos_proposed_id.value = self.id.value
        else:
            self.paxos_proposed_id.value = \
                self.paxos_proposed_id.value + self.num_nodes.value

    def prepare(self, proposed_id):
        if p() < self.prob:
            return
        if proposed_id.value <= self.paxos_promised_id.value:
            return None
        else:
            self.paxos_promised_id.value = proposed_id.value

            if self.paxos_accepted_id.value is not None and \
                            self.paxos_accepted_id.value != -1:
                return self.paxos_promised_id.value, \
                       self.paxos_accepted_id.value, \
                       self.paxos_accepted_value.value
            else:
                return self.paxos_promised_id.value

    def save_value(self, proposed_value):
        while True:
            responses = []
            self.generate_next_paxos_id()

            for _id, node in self.nodes.items():
                logger.debug('node {}: sending PREPARE(id {}) to node id {}'.format(
                    self.id.value, self.paxos_proposed_id, node.id.value
                ))
                responses.append(node.prepare(self.paxos_proposed_id))

            logger.debug('''node {}: checking for majority for proposed-id {}, responses {}'''.format(
                self.id.value, self.paxos_proposed_id.value, responses
            ))
            if self.check_majority(responses):
                logger.debug('''node {}: majority for proposed-id {} achieved, sending accept-requests'''.format(
                    self.id.value, self.paxos_proposed_id.value
                ))
                self.send_accept_requests(responses, proposed_value)
                return

    def accept_request(self, paxos_proposed_id, paxos_proposed_value):
        if paxos_proposed_id.value < self.paxos_promised_id.value:
            return None
        else:
            self.paxos_accepted_id.value = paxos_proposed_id.value
            self.paxos_accepted_value.value = paxos_proposed_value.value

            return self.paxos_accepted_id.value, self.paxos_accepted_value.value

    def send_accept_requests(self, responses, proposed_value):
        accept_request_responses = []

        temp_id = -1
        for response in responses:
            if type(response) is tuple:
                if response[1] > temp_id:
                    self.paxos_proposed_value.value = response[2]
                    temp_id = response[1]

        if not self.paxos_proposed_value.value:
            self.paxos_proposed_value.value = proposed_value

        for _id, node in self.nodes.items():
            logger.debug('node {}: sending ACCEPT-REQUEST(id {}, value {}) to node id {}'.format(
                self.id.value, 
                self.paxos_proposed_id.value, 
                self.paxos_proposed_value.value, 
                node.id.value
            ))
            accept_request_responses.append(
                node.accept_request(self.paxos_proposed_id,
                                    self.paxos_proposed_value)
            )

        logger.debug('''node {}: checking for majority for proposed-id {}, accept-request-responses {}'''.format(
            self.id.value, self.paxos_proposed_id.value, accept_request_responses
        ))

        if self.check_majority(accept_request_responses):
            logger.info('NODE {} , PROCESS-ID {}: CONSENSUS IS REACHED ON VALUE: {}'.format(
                self.id.value, os.getpid(), self.paxos_accepted_value.value
            ))

            for _id, node in self.nodes.items():
                node.data_store[self.paxos_accepted_id.value] = self.paxos_accepted_value.value

            for _id, node in self.nodes.items():
                node.paxos_promised_id.value = -1
                node.paxos_proposed_value.value = None
                node.paxos_accepted_id.value = -1
                node.paxos_accepted_value.value = None

            logger.info('node-id {}: current state of data-store: {}'.format(self.id.value, self.data_store))
        else:
            self.save_value(proposed_value)

    def check_majority(self, responses):
        if len(responses) == self.num_nodes.value:
            response_count = self.num_nodes.value
        else:
            response_count = self.num_nodes.value
        response_count -= responses.count(None)

        if response_count >= self.majority.value:
            return True
        else:
            return False
